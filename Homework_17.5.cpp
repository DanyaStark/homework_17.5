﻿#include <iostream>

class Vector 
{
private:
    double x = 0;
    double y = 0;
    double z = 0;

    double tempX = 0;
    double tempY = 0;
    double tempZ = 0;
    double sum = 0;
    double vecLength = 0;

public:
    void VecOutput()
    {
        std::cout << "Vector = (" << x << ";" << y << ";" << z << ")" << "\n";
    }

    int VecInput(int InpX, int InpY, int InpZ)
    {
        x = InpX;
        y = InpY;
        z = InpZ;
        return 0;
    }

    int VecLength()
    {
        tempX = x * x;
        tempY = y * y;
        tempZ = z * z;

        sum = tempX + tempY + tempZ;
        vecLength = sqrt(sum);

        std::cout << "Vector Length = " << vecLength;

        return 0;
    }
};

int main()
{
    int x;
    int y;
    int z;

    std::cout << "Please, enter vector coordinates" << std::endl;
    std::cout << "x = ";
    std::cin >> x;
    std::cout << "y = ";
    std::cin >> y;
    std::cout << "z = ";
    std::cin >> z;

    Vector temp;
    temp.VecInput(x, y, z);
    temp.VecOutput();
    temp.VecLength();
}